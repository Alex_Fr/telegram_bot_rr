from .player import Player
import requests
from bs4 import BeautifulSoup
import datetime
import time
import asyncio
# from threading import Timer
# from time import sleep


class Manager:

    def __init__(self, login, password):

        self.params = {
            'username': login,
            'password': password,
            'login': '&#1051;&#1086;&#1075;&#1080;&#1085;'
        }

        self.auth_url = 'http://rockingrackets.com/index.php'
        self.my_players_url = 'index.php?page=yourplayers'
        self.free_player_url = 'index.php?page=players&subpage=free&subsubpage=14-&sort=talent&start=1'
        self.players = []
        self.trainers = []
        self.worlds = {}

    def auth(self):

        session = requests.Session()
        response = session.post(self.auth_url, self.params)
        return session

    def add_trainers(self, tables_my_players, rr_n, number):

        trainer_url = tables_my_players[number].find('a', class_='self')['href']
        id_trainer = trainer_url.split('=')[-1]
        self.trainers.append(Player(rr_n, 'trainer', id_trainer, trainer_url))

    def add_players(self, tables_my_players, rr_n):

        players = tables_my_players[0].find_all('a', class_='self')

        for player in players:
            id_ = player['href'].split('=')[-1]
            url = player['href']
            self.players.append(Player(rr_n, 'player', id_, url))

    def search_my_worlds(self, response):

        soup = BeautifulSoup(response.text, "html.parser")
        tr = soup.find('div', class_='content').find('table', class_='realtable').find_all('tr')[1:]
        for item in tr:
            link = item.find('td').find('a')
            self.worlds['rr' + link.text.split(' ')[-1]] = link['href'] + '/'

    def search_players_and_trainers(self):

        session = requests.Session()
        response = session.post(self.auth_url, self.params)
        self.search_my_worlds(response)

        for name in self.worlds:
            self.search(session, self.worlds[name])

    def search(self, session, rr_n):

        data = session.get(rr_n + self.my_players_url)
        soup = BeautifulSoup(data.text, "html.parser")
        tables_my_players = soup.find_all('table', class_='realtable')

        if len(tables_my_players) != 2:

            if len(tables_my_players) == 4:
                # url, id игроков, id тренера. тут есть и игроки и тренер :
                self.add_players(tables_my_players, rr_n)
                self.add_trainers(tables_my_players, rr_n, 1)
            else:
                text = soup.find_all('p')[2].text

                if text:
                    # есть только тренер
                    self.add_trainers(tables_my_players, rr_n, 0)
                else:
                    # тренера нет. но есть игроки
                    self.add_players(tables_my_players, rr_n)

    def calculate_value_my_players(self):

        session = self.auth()

        for p in self.players:
            data = session.get(p.rr_n + p.url + '&match_type=tournament&addb=player&addv=' + p.id_)
            soup = BeautifulSoup(data.text, "html.parser")

            p.name = soup.find('div', class_='content').find_all('a')[1].text

            all_td = soup.find('table').find_all('td', class_='value')
            p.age_percent = round(float(all_td[0].find('span')['title'].split(' ')[-1][1:-2]) / 100.0, 2)
            p.talent = round(float(all_td[5].find('div')['title']), 2)
            p.power = round(float(all_td[6].find('div')['title']) / p.age_percent, 2)
            p.speed = round(float(all_td[7].find('div')['title']) / p.age_percent, 2)
            p.charisma = round(float(all_td[8].find('div')['title']), 2)
            p.stamina = round(float(all_td[9].find('div')['title']) / pow(p.age_percent, 2), 2)
            p.value = round((p.power + p.stamina + p.speed), 2)
            p.form = round(float(all_td[-2].find('div')['title'].split(' ')[1]), 1)

    def where_is_my_monday(self, rr_n):

        session = self.auth()

        page = session.get(rr_n + self.free_player_url)
        soup = BeautifulSoup(page.text, 'html.parser')

        day = soup.find('div', class_='serveranddate').text.split(' ')[-2]

        time_string = soup.find('div', class_='serveranddate').text.split(' ')[-3][1:-2]
        date_time = datetime.datetime.strptime(time_string, "%H:%M:%S")
        a_timedelta = date_time - datetime.datetime(1900, 1, 1)
        seconds = a_timedelta.total_seconds()
        if seconds != 0.0:
            if day != 'Воскресенье':
                seconds += 1200
                print(seconds)
                asyncio.sleep(seconds - 1)
                session = self.auth()
                res = self.search_new_favorite(session, rr_n)
                print(res)
            else:
                time_string = soup.find('div', class_='serveranddate').text.split(' ')[-1][1:-1]
                date_time = datetime.datetime.strptime(time_string, "%H:%M:%S")
                a_timedelta = date_time - datetime.datetime(1900, 1, 1)
                seconds = a_timedelta.total_seconds()
                print(seconds)
                asyncio.sleep(seconds - 1)
                session = self.auth()
                res = self.search_new_favorite(session, rr_n)
                print(res)

    def wait(self, session, rr_n):
        print('wait')
        page = session.get(rr_n + self.free_player_url)
        soup = BeautifulSoup(page.text, 'html.parser')

        day = soup.find('div', class_='serveranddate').text.split(' ')[-2]
        time_str = soup.find('div', class_='serveranddate').text.split(' ')[-1][1:-1]
        if day != 'Понедельник' or time_str == '0:00:00' or time_str == 'Обновление':
            return True
        else:
            return False

    def search_new_favorite(self, session, rr_n):

        # session = self.auth()
        flag = True

        while(flag):
            flag = self.wait(session, rr_n)
            time.sleep(1)

        data = session.get(rr_n + self.free_player_url)
        soup = BeautifulSoup(data.text, "html.parser")

        players = soup.find('div', class_='content').find_all('table', class_='realtable')[1].find_all('tr')[1:]

        free_p = []
        for player in players:
            properties = player.find_all('td', attrs={'style': False})

            if len(properties) == 13:
                if float(properties[4].text) >= 4.0:
                    if float(properties[-1].text) == 0:
                        free_p.append(properties)
                else:
                    break

        if free_p:

            favorite = None

            for p in free_p:
                data = session.get(rr_n + p[0].find('a')['href'])
                soup = BeautifulSoup(data.text, "html.parser")
                age_coeff = float(soup.find(
                    'table', class_='player_infotable').find_all('tr')[2].text.split(' ')[1][1:-2]) / 100
                url = p[0].find('a')['href']
                power = round(float(p[5].text) / age_coeff, 2)
                speed = round(float(p[6].text) / age_coeff, 2)
                charisma = round(float(p[7].text), 2)
                stamina = round(float(p[8].text) / pow(age_coeff, 2), 2)
                value = round(power + stamina + speed, 2)

                # if (stamina + power >= 7.0) and stamina >= 3.9 and speed >= 3.0 and power >= 3.0:
                if (stamina + power >= 3.5) and stamina >= 1.7 and speed >= 1.7 and power >= 1.7 and charisma >= 1.7:
                    if favorite:
                        # if stamina > favorite[4] and speed >= 3.0 and power >= 3.0 and (power + stamina) >= (favorite[2] + favorite[4]):
                        if stamina > favorite[4] and speed >= 2.0 and power >= 2.0:
                            favorite = [url, age_coeff, power, speed, stamina, charisma, value]
                    else:
                        favorite = [url, age_coeff, power, speed, stamina, charisma, value]

        else:
            return 'достойных не нашлось (free_p)'

        # тут теперь нужно сравнить выбранного игрока если он нашелся со своим самым слабым (если слотов нет)
        # и заменить если новый лучше

        # допустим есть свободный слот и новый игрок нашелся. необходимо нанять нового
        # игрока и добавить его в список своих игроков а также
        # просчитать его остальные свойства.
        n = 0
        pl = []
        for player in self.players:
            if player.rr_n == rr_n:
                n += 1
                pl.append(player)

        if favorite:

            params_add = {
                'player_id': favorite[0].split('=')[-1],
                'hire': '&#1053;&#1072;&#1085;&#1103;&#1090;&#1100; &#1079;&#1072; 40 &#1086;&#1095;&#1082;&#1086;&#1074;'
            }

            if n <= 1:

                response = session.post(rr_n + favorite[0] + '&match_type=tournament', params_add)

            else:
                # значит их 2 и надо удалять сначала более слабого и после нанимать
                # нового, если конечно он лучше

                if pl[0].stamina > pl[1].stamina:
                    low_player = pl[1]
                else:
                    low_player = pl[0]

                if favorite[4] > low_player.stamina:
                    params_del = {
                        'player_id': low_player.id_,
                        'fire': '&#1059;&#1074;&#1086;&#1083;&#1080;&#1090;&#1100;'}

                    # удаление
                    session.post(rr_n + self.my_players_url, params_del)

                    # затем нанимаем на освободившийся слот
                    response = session.post(rr_n + favorite[0] + '&match_type=tournament', params_add)

            self.players = []
            self.trainers = []
            self.search_players_and_trainers()
            self.calculate_value_my_players()

            return favorite

        else:
            return 'достойных не нашлось'
