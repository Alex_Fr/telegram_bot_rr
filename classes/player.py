class Player:

    def __init__(self, server, type_, id_, url):
        self.rr_n = server
        self.type_ = type_
        self.id_ = id_
        self.url = url

        self.name = None
        self.age_percent = None
        self.talent = None
        self.form = None
        self.power = None
        self.stamina = None
        self.speed = None
        self.charisma = None
        self.value = None

    # def get_player_info(self):
    #     p = self
    #
    #     info = '''
    #     Имя Фамилия : {0}
    #     Талант : {1}
    #     Выносливость : {2}
    #     Сила : {3}
    #     Скорость : {4}
    #     Харизма : {5}
    #     Общая ценность  : {6}
    #     Форма : {7}
    #     Процент (возраст) : {8}
    #     '''
    #     print('=' * 50)
    #     print(info.format(p.name,
    #                       p.talent,
    #                       p.stamina,
    #                       p.power,
    #                       p.speed,
    #                       p.charisma,
    #                       p.value,
    #                       p.form,
    #                       p.age_percent))
    #     print('=' * 50)
