import random

from aiogram import types
from asyncpg import Connection, Record
from asyncpg.exceptions import UniqueViolationError

from loader import bot, dp, db
from states.test import Question
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Command
import logging


class DBCommands:
    pool: Connection = db

    ADD_NEW_MANAGER = "INSERT INTO managers(chat_id, login, password) " \
                      "VALUES ($1, $2, $3) RETURNING id"

    GET_MANAGERS = "SELECT chat_id, login, password FROM managers"

    ADD_NEW_USER_REFERRAL = "INSERT INTO users(chat_id, username, full_name, referral) " \
                            "VALUES ($1, $2, $3, $4) RETURNING id"
    ADD_NEW_USER = "INSERT INTO users(chat_id, username, full_name) VALUES ($1, $2, $3) RETURNING id"
    COUNT_USERS = "SELECT COUNT(*) FROM users"
    GET_ID = "SELECT id FROM users WHERE chat_id = $1"
    CHECK_REFERRALS = "SELECT chat_id FROM users WHERE referral=" \
                      "(SELECT id FROM users WHERE chat_id=$1)"
    CHECK_BALANCE = "SELECT balance FROM users WHERE chat_id = $1"
    ADD_MONEY = "UPDATE users SET balance=balance+$1 WHERE chat_id = $2"

    async def add_new_manager(self, login, password):

        user = types.User.get_current()
        chat_id = user.id

        args = chat_id, login, password

        command = self.ADD_NEW_MANAGER

        try:
            record_id = await self.pool.fetchval(command, *args)
            return record_id
        except UniqueViolationError:
            pass

    # async def get_managers(self):
    #
    #     command = self.GET_MANAGERS
    #     rows = await self.pool.fetch(command)
    #
    #     return rows

        # return ", ".join([
        #     f"{num + 1}. " + str(manager['chat_id']) + ':' + manager['login'] + ':' + manager['password']
        #     for num, manager in enumerate(rows)
        # ])







    async def add_new_user(self, referral=None):
        user = types.User.get_current()

        chat_id = user.id
        username = user.username
        full_name = user.full_name
        args = chat_id, username, full_name

        if referral:
            args += (int(referral),)
            command = self.ADD_NEW_USER_REFERRAL
        else:
            command = self.ADD_NEW_USER

        try:
            record_id = await self.pool.fetchval(command, *args)
            return record_id
        except UniqueViolationError:
            pass

    async def count_users(self):
        record: Record = await self.pool.fetchval(self.COUNT_USERS)
        return record

    async def get_id(self):
        command = self.GET_ID
        user_id = types.User.get_current().id
        return await self.pool.fetchval(command, user_id)

    async def check_referrals(self):
        user_id = types.User.get_current().id
        command = self.CHECK_REFERRALS
        rows = await self.pool.fetch(command, user_id)
        return ", ".join([
            f"{num + 1}. " + (await bot.get_chat(user["chat_id"])).get_mention(as_html=True)
            for num, user in enumerate(rows)
        ])

    async def check_balance(self):
        command = self.CHECK_BALANCE
        user_id = types.User.get_current().id
        return await self.pool.fetchval(command, user_id)

    async def add_money(self, money):
        command = self.ADD_MONEY
        user_id = types.User.get_current().id
        return await self.pool.fetchval(command, money, user_id)


db = DBCommands()


@dp.message_handler(Command("register"), state=None)
async def enter_register(message: types.Message):
    await message.answer("Регистрация.\n\n"
                         "Введите ваш логин от сайта RockingRackets:")
    # Вариант 1 - с помощью функции сет
    await Question.Q1.set()


@dp.message_handler(state=Question.Q1)
async def answer_q1(message: types.Message, state: FSMContext):
    answer = message.text

    # Вариант  - передаем как словарь
    await state.update_data(
        {'login': answer}
    )

    await message.answer("Введите ваш пароль от RR:")

    await Question.next()


@dp.message_handler(state=Question.Q2)
async def answer_q2(message: types.Message, state: FSMContext):
    # Достаем переменные
    data = await state.get_data()
    login = data.get('login')
    password = message.text
    # chat_id = message.chat.id
    # теперь надо добавить в таблицу запись с этими данными
    id = await db.add_new_manager(login, password)

    text = ""
    if not id:
        # id = await db.get_id()
        text += 'Вы уже регистрировались ранее!'
    else:
        text += "Вы успешно зарегистрированы!"

    await message.answer(text)

    await state.finish()


# @dp.message_handler(commands=["get_managers"])
# async def check_managers(message: types.Message):
#     managers = await db.get_managers()
#     # text = f"Ваши менеджеры:\n{managers}"
#
#
#
#     text = 'подготовительные функции для менеджеров запущены'
#     await message.answer(text)






@dp.message_handler(commands=["start"])
async def register_user(message: types.Message):
    chat_id = message.from_user.id
    referral = message.get_args()
    id = await db.add_new_user(referral=referral)
    count_users = await db.count_users()

    text = ""
    if not id:
        id = await db.get_id()
    else:
        text += "Записал в базу! "

    bot_username = (await bot.me).username
    bot_link = f"https://t.me/{bot_username}?start={id}"
    balance = await db.check_balance()
    text += f"""
Сейчас в базе {count_users} человек!
Ваша реферальная ссылка: {bot_link}
Проверить рефералов можно по команде: /referrals
Ваш баланс: {balance} монет.
Добавить монет: /add_money
"""

    await bot.send_message(chat_id, text)


@dp.message_handler(commands=["referrals"])
async def check_referrals(message: types.Message):
    referrals = await db.check_referrals()
    text = f"Ваши рефералы:\n{referrals}"

    await message.answer(text)


@dp.message_handler(commands=["add_money"])
async def add_money(message: types.Message):
    random_amount = random.randint(1, 100)
    await db.add_money(random_amount)
    balance = await db.check_balance()

    text = f"""
Вам было добавлено {random_amount} монет.
Теперь ваш баланс: {balance}
    """
    await message.answer(text)



