from .help import dp
from .start import dp
from .echo import dp
from .testing import dp
from .handlers import dp

__all__ = ["dp"]
