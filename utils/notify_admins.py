import logging
import asyncio
from sql import create_db
from aiogram import Dispatcher
from config import ADMINS

from asyncpg import Connection
from loader import db, MANAGERS
from classes.manager import Manager


async def on_startup_notify(dp: Dispatcher):
    # Подождем пока запустится база данных...
    await asyncio.sleep(10)
    await create_db()

    await asyncio.sleep(10)
    # теперь нужно обратится к таблице менеджеров и достать их данные для запуска
    # подготовительных функций

    try:
        pool: Connection = db
        get_managers_command = "SELECT chat_id, login, password FROM managers"
        managers = await pool.fetch(get_managers_command)

        for manager in managers:
            MANAGERS[str(manager['chat_id'])] = Manager(manager['login'], manager['password'])

        # ПОМНИ!!! КЛЮЧИ ТЕПЕРЬ В СТРОКОВОМ ФОРМАТЕ. А В БАЗЕ ОНИ INT
        for manager in MANAGERS.values():
            manager.search_players_and_trainers()
            manager.calculate_value_my_players()
        logging.info("Objects managers created. functions: train and calculate value started")
    except Exception as err:
        logging.exception(err)
    # await asyncio.sleep(5)

    for admin in ADMINS:
        try:
            await dp.bot.send_message(admin, "Бот Запущен")
            await dp.bot.send_message(admin, MANAGERS[str(admin)].players[0].name)
        except Exception as err:
            logging.exception(err)
