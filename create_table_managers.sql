create table if not exists managers
(
    chat_id   bigint            not null
        constraint managers_pk
            primary key,
    login     text,
    password  text,
    id        serial            not null
);

alter table managers
    owner to postgres;

create unique index if not exists managers_id_uindex
    on managers (id);
