from loader import bot, storage
# import middlewares, filters, handlers
from utils.notify_admins import on_startup_notify
# import requests

async def on_shutdown(dp):
    await bot.close()
    await storage.close()


if __name__ == '__main__':
    from aiogram import executor
    from handlers import dp

    executor.start_polling(dp, on_shutdown=on_shutdown, on_startup=on_startup_notify)






# from aiogram import executor
#
# from loader import dp
# import middlewares, filters, handlers
# from utils.notify_admins import on_startup_notify
#
#
# async def on_startup(dispatcher):
#     # Уведомляет про запуск
#     await on_startup_notify(dispatcher)
#
#
# if __name__ == '__main__':
#     executor.start_polling(dp, on_startup=on_startup)
